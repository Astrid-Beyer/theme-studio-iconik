<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package iconik_studio
 */

get_header();
?>

	<main id="primary" class="site-main">

		<section class="error-404 not-found">

			<div class="page-content">
				<h2><?php esc_html_e( 'There is nothing here.', 'iconik_studio' ); ?></h2>
				<p>We suggest you to <a class="bold" href="<?php echo get_bloginfo('url') ?>">go back</a>.</p>

			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();

