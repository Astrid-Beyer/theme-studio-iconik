<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package iconik_studio
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:title" content="<?php echo get_bloginfo() ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo get_bloginfo('url') ?>" />
	<meta property="og:image" content="<?php echo get_theme_mod('embed') ?>" />
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e('Skip to content', 'iconik_studio'); ?></a>

		<header id="masthead" class="site-header section">


			<nav id="site-navigation" class="main-navigation bold">
				<input id="burger-toggle" type="checkbox" hidden />
				<label class="burger" for="burger-toggle"></label>
				<?php echo '<img src="' . get_bloginfo('template_url') . '/img/Logo.svg" class="logo_nav"';
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu'
					)
				);
				?>
			</nav><!-- #site-navigation -->
			<div id="site-navigation-right" class="nav-right bold">
				<nav class="flag"><img class="flag-img" src="<?php
				if (get_locale() == 'fr_FR')
					echo bloginfo('template_url') . "/img/fr.svg\" title='switch in english'";
				else
					echo bloginfo('template_url') . "/img/gb.svg\" title='basculer en français'";
				?>" alt="change language"></nav>
			</div><!-- #site-navigation-right -->
		</header><!-- #masthead -->