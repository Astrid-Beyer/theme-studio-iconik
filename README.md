# Thème WordPress ICONIK_Studio

Un thème conçu pour le site du studio Iconik.

## Comment installer ce thème ?
1. Se rendre sur son tableau de bord WP, Apparence > Thèmes et cliquer sur "Ajouter un thème".
2. Cliquer sur "Téléverser un thème" et choisir Fichier, selectionner le fichier .zip du thème. Cliquer sur Installer maintenant.
3. Cliquer sur Activer pour utiliser le nouveau thème.

## Infos complémentaires
Ajouter tout d'abord un **Menu Primary** sur l'interface Wordpress (bouton "Personnaliser" quand on est connecté en tant qu'administrateur) avec toutes les pages nécessaires pour que la navbar comporte les éléments souhaités. 

La page d'accueil doit être une **page statique** (voir panneau d'administration WP, Réglages, Lecture, cocher "La page d'accueil affiche **une page statique**) dotée de la template "Page d'Accueil". Nommer cette page avec le titre du jeu.

Pour mettre une template sur une page :
> Modifier la page;
> Dans l'onglet page, se rendre sur le menu **Modèle**
> Sélectionner dans le menu déroulant la template associée à la page

Il est nécessaire de créer un **Menu** "Footer" pour le footer également afin d'y glisser les mentions légales, politique de confidentialité...

Si besoin, dans le fichier `.htaccess` (fichier root du Wordpress), ajouter les lignes suivantes afin de pouvoir publier des images de grande taille :

```
php_value upload_max_filesize 64M
php_value post_max_size 128M
php_value memory_limit 256M
php_value max_execution_time 300
php_value max_input_time 300
```

## Passer le site en HTTPS

Se rendre d'abord sur le back de WordPress, Réglages > Général, et dans les champs "Adresse web de WordPress (URL)" et "Adresse web du site (URL)", remplacer "http" par "https".

Ensuite, modifier le fichier .htacess situé à la racine de chacun des sites pour écrire :

```
# Begin Force HTTPS
RewriteEngine On
RewriteCond %{HTTPS} !=on
RewriteRule ^/?(.*) https://URL_DU_SITE/$1 [R,L]
# End Force HTTPS
```

Où URL_DU_SITE peut être "studio.iconik.com" par exemple.

Faire CTRL+F5 pour nettoyer le cache navigateur en rafraichissant la page pour vérifier que le site est bien passé en HTTPS.

# Crédits
* Based on Underscores https://underscores.me/, (C) 2012-2020 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2018 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)

Thème réalisé avec ♥ pour Iconik© par Astrid BEYER, 2022