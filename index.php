<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package iconik_studio
 */

get_header();
?>

<main id="primary" class="site-main">

	<?php if (is_home() && is_front_page()) :	?>
		<section>
		<h1>Hello world !</h1>
		<p>Ce thème a été conçu pour les sites de jeu vidéos d'Iconik. Suivre le process pour mettre en place le thème correctement.</p>
		
		<p>Lien utile : <a href="https://gitlab.com/Astrid-Beyer/theme-wp-iconik/-/blob/main/README.md">README</a></p>
		</section>

<?php endif; ?>

</main><!-- #main -->

<?php
get_footer();
