<?php

/**
 * iconik_studio functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package iconik_studio
 */

if (!defined('ICONIK_STUDIO_VERSION')) {
	// Replace the version number of the theme on each release.
	define('ICONIK_STUDIO_VERSION', '1.0.0');
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function iconik_studio_setup()
{
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on iconik_studio, use a find and replace
		* to change 'iconik_studio' to the name of your theme in all the template files.
		*/
	load_theme_textdomain('iconik_studio', get_template_directory() . '/languages');

	// Add default posts and comments RSS feed links to head.
	add_theme_support('automatic-feed-links');

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support('title-tag');

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support('post-thumbnails');

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__('Primary', 'iconik_studio'),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support('customize-selective-refresh-widgets');

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action('after_setup_theme', 'iconik_studio_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function iconik_studio_content_width()
{
	$GLOBALS['content_width'] = apply_filters('iconik_studio_content_width', 640);
}
add_action('after_setup_theme', 'iconik_studio_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function iconik_studio_widgets_init()
{
	register_sidebar(
		array(
			'id'            => 'histoire',
			'name'          => esc_html__('Histoire', 'iconik_studio'),
			'description'   => esc_html__('Add widgets in main content.', 'iconik_studio'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		)
	);
	register_sidebar(
		array(
			'id'            => 'key_features',
			'name'          => esc_html__('Key-Features', 'iconik_studio'),
			'description'   => esc_html__('Add widgets in keyfeatures content.', 'iconik_studio'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
}
add_action('widgets_init', 'iconik_studio_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function iconik_studio_scripts()
{
	wp_enqueue_style('iconik_studio-style', get_stylesheet_uri(), array(), ICONIK_STUDIO_VERSION);
	wp_style_add_data('iconik_studio-style', 'rtl', 'replace');

	wp_enqueue_script('navbar-js',  get_template_directory_uri() . '/js/navbar.js', array('jquery'), '1.0.0');
	wp_enqueue_script('scriptDelay-js',  get_template_directory_uri() . '/js/scriptDelay.js', array('jquery'), false, true);

	wp_enqueue_script('iconik_studio-navigation', get_template_directory_uri() . '/js/navigation.js', array(), ICONIK_STUDIO_VERSION, true);

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'iconik_studio_scripts');


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Customization!! 
 * 
 * All our sections, settings, and controls will be added here
 * https://developer.wordpress.org/themes/customize-api/customizer-objects/
 * 
 */

function customize_register($wp_customize)
{
	/**
	 * IMAGE EMBED
	 */

	$wp_customize->add_setting('embed', array(
		'sanitize_callback' => 'esc_url_raw',
	)); // setting

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'embed_control', array(
		'section' => 'title_tagline',
		'settings' => 'embed',
		'label' => __('Remplacer l\'image du site quand on partage son URL'),
		'button_labels' => array( // All These labels are optional
			'select' => 'Select img',
			'remove' => 'Remove img',
			'change' => 'Change img',
		)
	))); // control


	/*** Réseaux sociaux dans le footer 
	 * ------------------------------------------------***/
	$wp_customize->add_section('footer', array(
		'title'      => __('Réseaux footer', 'iconik_studio'),
		'description' => 'Laisser un champ vide pour retirer un réseau',
		'priority'   => 30,
	));

	// twitter
	$wp_customize->add_setting('footer-twt', array(
		'default'   => '', /* https://twitter.com/IconikHybrid */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_twt', array(
		'label'      => __('Lien Twitter', 'iconik_studio'),
		'section'    => 'footer',
		'settings'   => 'footer-twt',
	));

	// instagram
	$wp_customize->add_setting('footer-ig', array(
		'default'   => '', /* https://www.instagram.com/iconik.hybridmedia/ */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_ig', array(
		'label'      => __('Lien instagram', 'iconik_studio'),
		'section'    => 'footer',
		'settings'   => 'footer-ig',
	));

	// facebook
	$wp_customize->add_setting('footer-fb', array(
		'default'   => '', /* https://www.facebook.com/iconik.hybridmedia/  */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_fb', array(
		'label'      => __('Lien Facebook', 'iconik_studio'),
		'section'    => 'footer',
		'settings'   => 'footer-fb',
	));

	// youtube
	$wp_customize->add_setting('footer-yt', array(
		'default'   => '', /* https://www.youtube.com/iconik */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_yt', array(
		'label'      => __('Lien YouTube', 'iconik_studio'),
		'section'    => 'footer',
		'settings'   => 'footer-yt',
	));

	// linkedin
	$wp_customize->add_setting('footer-li', array(
		'default'   => '', /* https://mp.linkedin.com/company/iconikhybridmedia */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_li', array(
		'label'      => __('Lien LinkedIn', 'iconik_studio'),
		'section'    => 'footer',
		'settings'   => 'footer-li',
	));

	// discord
	$wp_customize->add_setting('footer-disc', array(
		'default'   => '', /* https://discord.gg/iconik */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_disc', array(
		'label'      => __('Lien Discord', 'iconik_studio'),
		'section'    => 'footer',
		'settings'   => 'footer-disc',
	));

	// steam
	$wp_customize->add_setting('footer-steam', array(
		'default'   => '', /* https://store.steampowered.com/developer/ICONIK */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_steam', array(
		'label'      => __('Lien Steam', 'iconik_studio'),
		'section'    => 'footer',
		'settings'   => 'footer-steam',
	));
}
add_action('customize_register', 'customize_register');
