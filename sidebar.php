<?php

/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package iconik_studio
 */

?>
	<p class="site-description bold">
		<?php
		$iconik_studio_description = get_bloginfo('description', 'display');
		echo $iconik_studio_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped 
		?>
	</p>