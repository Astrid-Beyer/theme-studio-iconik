<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package iconik_studio
 */

?>

<footer id="colophon" class="site-footer">
	<div class="logo-iconik">
		<a href="https://www.iconik.com/" target="_blank"><img class="footer-logo" src="<?php bloginfo('template_url'); ?>/img/Logo_ICONIKBLANC.png" alt="Logo Iconik"></a>
		<span class="catchphrase">PLAY WITH EMOTIONS</span>
	</div>
	<div class="social-networks">
		<!-- ajoutable manuellement dans menu custom -->
		<?php
		$result = '';

		if (get_theme_mod('footer-twt')) {
			$result .= '<a class="network-bottom" href="' . get_theme_mod('footer-twt', 'no value') . '" target="_blank"><img src="' . get_bloginfo('template_url') . '/img/twitter.png" alt="twitter"></a>';
		}

		if (get_theme_mod('footer-ig')) {
			$result .= '<a class="network-bottom" href="' . get_theme_mod('footer-ig', 'no value') . '" target="_blank"><img src="' . get_bloginfo('template_url') . '/img/instagram.png" alt="instagram"></a>';
		}

		if (get_theme_mod('footer-fb')) {
			$result .= '<a class="network-bottom" href="' . get_theme_mod('footer-fb', 'no value') . '" target="_blank"><img src="' . get_bloginfo('template_url') . '/img/facebook.png" alt="facebook"></a>';
		}

		if (get_theme_mod('footer-yt')) {
			$result .= '<a class="network-bottom" href="' . get_theme_mod('footer-yt', 'no value') . '" target="_blank"><img src="' . get_bloginfo('template_url') . '/img/youtube.png" alt="youtube"></a>';
		}

		if (get_theme_mod('footer-li')) {
			$result .= '<a class="network-bottom" href="' . get_theme_mod('footer-li', 'no value') . '" target="_blank"><img src="' . get_bloginfo('template_url') . '/img/linkedin.png" alt="linkedin"></a>';
		}

		if (get_theme_mod('footer-disc')) {
			$result .= '<a class="network-bottom" href="' . get_theme_mod('footer-disc', 'no value') . '" target="_blank"><img src="' . get_bloginfo('template_url') . '/img/discord.png" alt="discord"></a>';
		}

		if (get_theme_mod('footer-steam')) {
			$result .= '<a class="network-bottom" href="' . get_theme_mod('footer-steam', 'no value') . '" target="_blank"><img src="' . get_bloginfo('template_url') . '/img/steam.png" alt="steam"></a>';
		}

		echo "$result";

		?>
	</div><!-- .social-networks -->
	<div class="bottom-line">
		<?php echo date("Y") ?> - Iconik. All rights reserved
		<nav id="footer-navigation" class="footer-navigation site-info">
			<input id="burger-toggle-footer" type="checkbox" hidden />
			<label class="burger-footer" for="burger-toggle-footer"></label>
			<?php
			wp_nav_menu(
				array(
					'menu_id' => 'Footer-menu'
				)
			);
			?>
		</nav><!-- nav -->
	</div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>