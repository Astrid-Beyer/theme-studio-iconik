/**
 * File navbar.js.
 *
 * Close the navbar reponsive menu when clicking away or on one link
 * Add a background to the navbar on scrolling
 */

(function ($) {
    document.addEventListener('click', event => {
        const divClicked = event.composedPath();
        if (
            !divClicked.some(div => $('.main-navigation').toArray().includes(div)) ||
            divClicked.some(div => $('.main-navigation .menu-primary-container ul li a').toArray().includes(div))
        ) $('#burger-toggle').prop('checked', false);
    });

    document.addEventListener('scroll', event => {
        let header = $("#masthead");
        let scroll = $(window).scrollTop();
        if (scroll >= 200)
            header.addClass('background-shadow');
        else
            header.removeClass('background-shadow');
    });
})(jQuery);
